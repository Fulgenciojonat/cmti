<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cmti' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'T55?B~uqgb%F?l%HDeU3d,th!_EEp.K9S|;C)!@FPvl#m7N]B#4N<&~qjdG|IHUA' );
define( 'SECURE_AUTH_KEY',  '9m+vAAaYYV/-Ckg^Sm!5dLOU{.PH}Mk8r@tddqa,*nN|&S!u^Sp(g0,*F52.ZHxE' );
define( 'LOGGED_IN_KEY',    '8d(],PP2[;4Z>d#^;o4v0p86S9PCT[Rk-|-YSv1fRQACPC<0Q019TXg2PWXG]Pu(' );
define( 'NONCE_KEY',        'cIt6|#:HKFw_dd:cxZdr-:`Tp7tm5&f%+SaW4<gcXL2D 2,}j/o:B<d~|-o{ QVM' );
define( 'AUTH_SALT',        'ZQZwz5`N=IVlGUd}m5N%@]u(&Z,|+>ZS&B4UX0|X{CeVEo?>q6Lf0)Uv2UBdCs>G' );
define( 'SECURE_AUTH_SALT', '$iG{ Yh[39W8$NjHWvKwoyMP%2use:;SyU2i;WI;x9!^/$!u6K6IUg:h#dTB>CuU' );
define( 'LOGGED_IN_SALT',   'L3c.^Ew<KrDr3CdPp/!&-u,-Bq{Ry74K=AP5,Zb+~rc1RUn#UmF&!pxe/#=b6:91' );
define( 'NONCE_SALT',       '[ ON14ONJZT=1UC<5|wrryE@EH+Ec$o`:9K`10(EkT)PI%jR/mozQ4+F+1:L{xA?' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
