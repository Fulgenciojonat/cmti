  $( document ).ready(function() {
       $(window).scroll(function () {
          var $nav = $('nav');
          var $scrolling = $(this).scrollTop();
          $nav.toggleClass('scrolled', $scrolling > $nav.height());
        });
   });