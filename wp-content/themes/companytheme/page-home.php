<?php 
/**
 * Top page
 *
 * Please create page on wordpress that slug is "home" and "news"
 * After that, Please change setting on "Settings" -> "Reading" -> "Your home page displays"
 * * A static page (select below)
 * * Homepage -> home
 * * Posts page -> news
 */

get_header();

wp_enqueue_style('homestyle', get_template_directory_uri() . '/assets/css/views/home.css', array());

 ?>

	 <div class="v-content">
	 	<div class="v-content-inner text-center">
 			<h4>Automation made easy for you</h4>
 			<a href="#" class="btn btn-blue">LEARN MORE</a>
 			<h1>GF PIPING SYSTEM</h1>
	 	</div>
	</div>
	<div class="v-section2">
		<div class="container">
			<h4 class="v-heading">Our Latest Projects</h4>
			<div class="row">
				<div class="col-lg-6 col-sm-12 v-heading-details">
					<h4>200 GPM Ultrapure Water System</h4>
					<a href="#"><img src="https://via.placeholder.com/150"></a>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
					
				</div>
				<div class="col-lg-6 col-sm-12 v-heading-details">
					<h4>150GPM ULTRAPURE WATER SYSTEMm</h4>
					<a href="#"><img src="https://via.placeholder.com/150"></a>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo..</p>
					
				</div>
			</div>
			
		</div>
	</div>
	<div class="v-section3">
		<div class="container">
			<h4 class="v-heading">Our Services</h4>
			<div class="row">
				<div class="col-lg-6 col-sm-12 v-heading-details text-center">
					<h4 class="text-center">SERVICE, MAINTENANCE AND REPLACEMENT OF PARTS</h4>

					<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo..</p>
					<a href="#" class="btn btn-blue">READ MORE</a>
					
				</div>
				<div class="col-lg-6 col-sm-12 v-heading-details text-center">
					<h4 class="text-center">WATER TREATMENT EQUIPMENT AND CONSUMABLES</h4>
					<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo..</p>
					<a href="#" class="btn btn-blue">READ MORE</a>
					
				</div>
			</div>
			
		</div>
	</div>
	 <div class="v-content">
	 	<div class="v-content-inner text-center">
 			<h4>EXPLORE THE WORLD</h4>
 			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 			xcepteur sint occaecat cupidatat non
 			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 			<a href="#" class="btn btn-blue">LEARN MORE</a>
	 	</div>
	</div>



 <?php

get_footer();

?>
