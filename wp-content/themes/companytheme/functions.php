<?php 

function cmti_script_enqueue(){
	
	wp_enqueue_style('basestyle', get_template_directory_uri() . '/assets/css/bootstrap.css', array(), '1.0.0', 'all');
	wp_enqueue_style('customstyle', get_template_directory_uri() . '/assets/css/base.css', array(), '1.0.0', 'all');
	wp_enqueue_script('jqueryjs', get_template_directory_uri() . '/assets/js/jquery-3.3.1.min.js', array(), '1.0.0', true);
	wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/assets/js/bootstrap.js', array(), '1.0.0', true);
	wp_enqueue_script('basejs', get_template_directory_uri() . '/assets/js//views/base.js', array(), '1.0.0', true);

}


add_action('wp_enqueue_scripts', 'cmti_script_enqueue');
